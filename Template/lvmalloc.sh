#!/bin/bash
################################################################################
# Allocates Logical Hard Disks for guests.
# use `fdisk -l` to determine /dev/sdaX device
################################################################################

use_string="-i device -s size -n name"
#   Use -i to initialize device. Creates physical volume group, xenia.
#   use -s and -n to create logical volume


usage() { echo "Usage $0 ${use_string}" 1>&2; exit 1; }
badno() { echo "ERROR: invalid size: ${n}" 1>&2; exit 2; }

## Parse Command Line Options ##
while getopts "i:s:n:" x; do
    case "${x}" in
        i) sdaX=${OPTARG} ;;
        s) n=${OPTARG} ;;
        n) name=${OPTARG} ;;
        *) usage ;;
    esac
done
shift $((OPTIND-1))

## Set Up LVM
if [ ! -z "${sdaX}" ]; then # Initializing
    pvcreate "/dev/${sdaX}"
    vgcreate xenia "/dev/${sdaX}"
fi

## Verify Args and Create Logical Volume
if [ -z "${n}" ] || [ -z "${name}" ]; then exit 0; fi
if [[ ! "${n}" =~ ^[0-9]+$ ]]; then badno; fi #NaN

# Assumes LVM is already Set Up:
lvcreate -L "${n}"G -n "${name}" /dev/xenia