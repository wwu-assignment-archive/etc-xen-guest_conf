#!/usr/bin/awk -f
################################################################################
#
#  cfgmaker -- change vif, vfb, disk, & name
#    Expects: tid, indx variables for team number, OS index
#        The OS index is used to differentiate VM guests based on the same conf file.
#
#   Note: The vncdisplay will be the concatenation of X, tid, inx
#       where x is a unique number for each os:
#           ubuntu 1, debian 2, windows7 3, vyos 4
#       tid is the team id number
#       inx is the index identifying differnt VMs with the same OS and Team
#   This will be prepended with 59 to select the port number
#   The highest possible port number is 65535, which is 5 digits.
#       which means that there can be no more
#           than 9 types of os, 9 teams, and 9 instances of the same os
#       TODO: determine a better strategy for assigning a unique vncdisplay

BEGIN { hvm=0 } # Assume Paravirtualized Guest

/name/ { print $0"."tid"."inx"\""; }

/vif/ { print $0" bridge=xenbr"tid"' ]"; }

/vfb/ { print $0 tid inx"' ]"; }

/vncdisplay/ && !/\[/ { print $0 tid inx; }

NF && !/name|vif|vfb|vncdisplay/ { print $0; }

/builder/ { hvm=1 } # Guest is Hardware Virtualized

/^disk/ { printf "%s%d%d,%s\n", $0, tid, inx, hvm==1? "hda,w']":"xvda,w']"; }