#!/bin/bash
################################################################################
#   mkteam.sh -- Creates and populates directory for team's configuration files.
# can use dryrun option [-x] and dump the commands > into a script
################################################################################

min_team_id=0
max_team_id=255
path_default="/etc/xen/guest_conf/Teams"
conf_path="/etc/xen/guest_conf/Template/conf"
confmaker="/etc/xen/guest_conf/Template/cfgmaker.awk"
network_interfaces_f="${path_default}/interfaces.append"
disk_name_list="${path_default}/nudisk.names"

use_string="-n team_number [-d dir] [-x] [OS list ...]"

usage() { echo "Usage $0 ${use_string}" 1>&2; exit 1; }
badno() { echo "ERROR: invalid team number: ${team_id}" 1>&2; exit 2; }

## Parse Command Line Options ##
while getopts "n:d:x" x; do
    case "${x}" in
        n) team_id=${OPTARG} ;;
        d) path=${OPTARG} ;;
        x) dryrun=true ;;
        *) usage ;;
    esac
done
shift $((OPTIND-1))
if [ -z "${team_id}" ]; then usage; fi
if [ -z "${dryrun}" ]; then dryrun=false
else echo "#!/bin/bash"; fi

## Verify team_id Validity
if [[ "${team_id}" =~ ^[0-9]+$ ]]; then
    if [ "${team_id}" -lt $min_team_id ]||[ "${team_id}" -gt $max_team_id ]
        then badno; fi #Out of Range
else badno; fi #NaN

## ASSERT DRY RUN WHILE TESTING
#dryrun=true

## Determine Directory for Team Files ##
if [ -z "${path}" ]; then path="${path_default}/${team_id}"
else path="${path}/${team_id}"; fi 
echo "# Team ${team_id} directory: \"${path}\""
if ${dryrun}; then echo "mkdir -p \"${path}\""
else mkdir -p "${path}"; fi

if [ -e "${disk_name_list}" ]; then touch "${disk_name_list}"; fi

## Gather Config Templates Needed for Scenario ##
for need in "$@"; do
    i=0
    dupcheck="${path}/${need}-${i}.cfg"
    confile="${conf_path}/${need}.cfg"
    if [ -e "${confile}" ]; then
        while [ -e "${dupcheck}" ]; do
            # Create Different File for Each machine with same OS
            # WARNING: Doesn't work in dryrun mode #XXX#TODO: FIX if !YAGNI
            i=$((i+1)) ; dupcheck="${path}/${need}-${i}.cfg"
        done
        if ${dryrun}; then
            echo "cat \"${confile}\" | \"${confmaker}\" -v tid=\"${team_id}\" -v inx=\"${i}\" >\"${dupcheck}\""
        else cat "${confile}" | "${confmaker}" -v tid="${team_id}" -v inx="${i}" >"${dupcheck}"; fi
        echo "${need:0:1}${team_id}${i}" >> "${disk_name_list}"
    else echo "# Missing Template config file \"${confile}\"" ; fi
done

## Create lvm partition or file for VM disks ##
##  OR a file ...???
# List of names for these disks are in ${disk_name_list} [from ln 64]

## Setup Subnet ##
# Create bridge interface:
#  iface xenbr${team_id}
#  bridge_ports none
#  address 10.0.${team_id}.0
#  netmask 255.255.255.0
#

if ${dryrun}; then
    echo "echo >> \"${network_interfaces_f}\""
    echo "echo \"#Team ${team_id} interface:\" >> \"${network_interfaces_f}\""
    echo "echo \"iface xenbr${team_id} inet static\" >> \"${network_interfaces_f}\""
    echo "echo \"bridge_ports none\" >> \"${network_interfaces_f}\""
    echo "echo \"address 10.0.${team_id}.0\" >> \"${network_interfaces_f}\""
    echo "echo \"netmask 255.255.255.0\" >> \"${network_interfaces_f}\""
else
    echo >> "${network_interfaces_f}"
    echo "#Team ${team_id} interface:" >> "${network_interfaces_f}"
    echo "iface xenbr${team_id} inet static" >> "${network_interfaces_f}"
    echo "bridge_ports none" >> "${network_interfaces_f}"
    echo "address 10.0.${team_id}.0" >> "${network_interfaces_f}"
    echo "netmask 255.255.255.0" >> "${network_interfaces_f}"
fi

#TODO: Create cleanup parameter which, removes nework_interfaces_f and disk_name_list,
# and if realclean also release disks and remove config files for teams.
# Maybe use makefile for this.